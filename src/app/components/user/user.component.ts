import {Component, Input, OnInit} from '@angular/core';
import {UserDataService} from "../../services/user-data/user-data.service";
import {User} from "../../interfaces/user-interface";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  @Input()
  user?: User

  constructor(protected userDataService: UserDataService) { }

  ngOnInit(): void {
    this.user = this.userDataService.loadDemoUser();
  }

  toString(): string {
    if (this.user)
      return `${this.user.firstName} ${this.user.lastName} aka. ${this.user.username} (${this.user.email}, ${this.user.pfp_url}) with groups ${this.user.groups}`;
    return ''
  }

}
