import { Component, OnInit } from '@angular/core';
import { Group } from 'src/app/interfaces/group-interface'

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {

  group?: Group

  constructor() { }

  ngOnInit(): void {
  }

  toString(): string {
    if (this.group)
      return `${this.group.name} (${this.group.description}): ${this.group.permissions}`;
    return '';
  }
}
