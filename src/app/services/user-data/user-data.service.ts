import { Injectable } from '@angular/core';
import {User} from "../../interfaces/user-interface";
import {GravatarService} from "../gravatar/gravatar.service";

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  constructor(protected gravatarService: GravatarService) { }

  loadDemoUser(): User {
    return {
      pfp_url: this.gravatarService.getUrlForEmail('admin@sgart.dev'),
      firstName: 'Samuel',
      lastName: 'Gartmann',
      username: 'sgart',
      email: 'admin@sgart.dev',
      groups: []
    }
  }

  isLoggedIn(): boolean {
    return true;
  }
}
