import { TestBed } from '@angular/core/testing';

import { GravatarService } from './gravatar.service';

describe('GravatarService', () => {
  let service: GravatarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GravatarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should create the correct md5 hash', () => {
    expect(service.getUrlForEmail('email@example.com')).toBe('https://www.gravatar.com/avatar/5658ffccee7f0ebfda2b226238b1eb6e')
  })
});
