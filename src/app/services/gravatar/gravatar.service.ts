import { Injectable } from '@angular/core';
import { MD5 } from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class GravatarService {

  constructor() { }

  getUrlForEmail(email: string): string {
    let emailHash = MD5(email);
    return `https://www.gravatar.com/avatar/${emailHash}`
  }
}
