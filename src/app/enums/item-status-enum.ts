export enum ItemStatus {
  reserved,
  in_use,
  free,
  in_repair,
  in_wash
}
