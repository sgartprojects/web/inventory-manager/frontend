export enum ItemConditionEnum {
  complete,
  incomplete,
  dirty,
  damaged,
  broken,
  unusable
}
