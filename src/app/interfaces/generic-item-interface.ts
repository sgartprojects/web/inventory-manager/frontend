export interface GenericItem {
  identifier: string,
  name: string,
  description: string,
  quantity: number
  unit_of_measurement: string
}
