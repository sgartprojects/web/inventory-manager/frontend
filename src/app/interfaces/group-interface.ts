import {Permission} from "./permission-interface";

export interface Group {
  name: string,
  description: string,
  permissions: Permission[]
}
